import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;


/**
 * EchoServer class
 * 
 * This is written mainly for TCP/UDP network program testing/debugging.
 * 
 * The need for this program stems from the fact that
 * there seems to be a lack of reliable, multi-threaded (for TCP) echo
 * server programs available. So I am writing my own.
 * 
 * Author: Ryan Bouchard,
 * Computer Engineering student at Rochester Institute of Technology
 * 
 * Initial Revision: 13 May, 2013
 * 
 * I hereby give permission for this program and its source code
 * to be freely modified, distributed, used for any purpose,
 * commercial or otherwise.
 * 
 */
public class EchoServer
{
	/**
	 * Maximum size for receiving buffer
	 */
	private static final int RECVBUF_SIZE = 16384;
	
	/**
	 * Flags
	 */
	private static boolean verbose, output, date, sender;
	
	/**
	 * Prints a packet according to flags
	 * @param data the packet data
	 * @param addr the remote address
	 * @param port the remote port
	 */
	private static void printPacket(byte[] data, InetAddress addr, int port)
	{
		StringBuilder out = new StringBuilder();
		
		if (date)
		{
			out.append(new Date().toString() + ": ");
		}
		if (sender)
		{
			out.append(addr.getHostAddress() + ":"
					+ port + ": ");
		}
		
		out.append(new String(data));
		
		System.out.println(out.toString());
	}
	
	/**
	 * TCPEcho class
	 * 
	 * Handles the connection between the echo client and server.
	 */
	private static class TCPEcho extends Thread
	{
		private Socket sock;
		
		/**
		 * Constructor
		 * @param sock the TCP socket on which to communicate
		 * @param verbose whether verbose or not
		 * @param output whether to output received data or not
		 */
		public TCPEcho(Socket sock)
		{
			this.sock = sock;
		}
		
		/**
		 * Thread run function
		 * 
		 * This handles the connection between incoming clients and the server.
		 * Will terminate when an exception occurs during socket IO.
		 */
		@Override
		public void run()
		{
			try
			{
				InputStream in = sock.getInputStream();
				OutputStream out = sock.getOutputStream();
				
				/* Loop forever until an IO exception occurs */
				for (;;)
				{
					/* Receive data from client */
					byte recv[] = new byte[RECVBUF_SIZE];
					int size = in.read(recv);
					
					if (size == -1)
					{
						throw new IOException("Connection closed.");
					}
					
					/* Write if output is on */
					if (output)
					{
						byte data[] = new byte[size];
						System.arraycopy(recv, 0, data, 0, data.length);
						
						if (output)
						{
							printPacket(data, sock.getInetAddress(),
										sock.getPort());
						}
						else
						{
							System.out.println(new String(data));
						}
					}
					
					/* Send data to client */
					out.write(recv, 0, size);
				}
			} catch (IOException e)
			{
				if (verbose)
				{
					System.out.println("Connection terminated: "
							+ e.getMessage());
				}
			} finally
			{
				/* Close socket */
				try
				{
					sock.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * UDPEcho function
	 * 
	 * Waits for incoming data on datagram socket and
	 * relays it back to the origin.
	 * 
	 * A thread is not necessary for UDP because it is connectionless.
	 * 
	 * Will terminate when an exception occurs during socket IO.
	 * 
	 * @param sock the UDP socket on which to send/receive data
	 */
	private static void UDPEcho(DatagramSocket sock)
	{
		try
		{
			/* Loop forever until an IO exception occurs */
			for (;;)
			{
				/* Receive data from client */
				DatagramPacket recv =
					new DatagramPacket(new byte[RECVBUF_SIZE], RECVBUF_SIZE);
				sock.receive(recv);
				
				/* Write if output is on */
				if (output)
				{
					if (output)
					{
						printPacket(recv.getData(), recv.getAddress(), recv.getPort());
					}
					else
					{
						System.out.println(new String(recv.getData()));
					}
				}
				
				/* Send data to client */
				sock.send(recv);
			}
		} catch (IOException e)
		{
			if (verbose)
			{
				System.out.println("IO exception sending/receiving packet: "
						+ e.getMessage());
			}
		} finally
		{
			/* Close socket */
			sock.close();
		}
	}
	
	/**
	 * Usage function
	 * 
	 * Displays command line usage
	 */
	private static void usage()
	{
		System.out.println("Usage: java EchoServer [options] protocol port");
		System.out.println("    options:");
		System.out.println("        -a, --address addr     IP address to bind on");
		System.out.println("        -v, --verbose          Verbose output");
		System.out.println("        -o, --output           Output received data");
		System.out.println("        -s, --sender           Print sender with packet (used with -o)");
		System.out.println("        -d, --date             Print date with packet (used with -o)");
		System.out.println("        -h, --help             Command line usage");
		System.out.println("    protocol:");
		System.out.println("        udp      UDP protocol");
		System.out.println("        tcp      TCP protocol");
	}
	
	/**
	 * Main function
	 * @param args command line arguments
	 */
	public static void main(String args[])
	{
		/* Get command line arguments */
		int nOpts = args.length - 2;
		
		if (nOpts < 0)
		{
			/* Invalid number of arguments */
			usage();
			return;
		}
		
		/* Parse options */
		InetAddress bindAddr = null;
		verbose = false;
		output = false;
		date = false;
		sender = false;
		
		for (int i = 0; i < nOpts; i++)
		{
			if (args[i].equals("-h") || args[i].equals("--help"))
			{
				/* Help */
				usage();
				return;
			}
			else if (args[i].equals("-a") || args[i].equals("--address"))
			{
				/* Need next argument */
				if (i == nOpts - 1)
				{
					usage();
					return;
				}
				
				/* Parse IP */
				i++;
				
				try
				{
					bindAddr = InetAddress.getByName(args[i]);
				} catch (UnknownHostException e)
				{
					usage();
					return;
				}
			}
			else if (args[i].equals("-v") || args[i].equals("--verbose"))
			{
				verbose = true;
			}
			else if (args[i].equals("-o") || args[i].equals("--output"))
			{
				output = true;
			}
			else if (args[i].equals("-d") || args[i].equals("--date"))
			{
				date = true;
			}
			else if (args[i].equals("-s") || args[i].equals("--sender"))
			{
				sender = true;
			}
			else
			{
				/* Invalid */
				usage();
				return;
			}
		}
		
		/* Get port */
		int port;
		
		try
		{
			port = Integer.parseInt(args[nOpts + 1]);
		} catch (NumberFormatException e)
		{
			usage();
			return;
		}
		
		/* Get protocol */
		String proto = args[nOpts];
		
		if (proto.equals("tcp"))
		{
			ServerSocket sock;
			
			/* Bind */
			try
			{
				if (bindAddr != null)
				{
					/* Address specified */
					sock = new ServerSocket(port, 0, bindAddr);
				}
				else
				{
					/* Address unspecified */
					sock = new ServerSocket(port);
				}
			} catch (IOException e)
			{
				System.err.println("Failed to bind: " + e.getMessage());
				return;
			}
			
			/* Accept forever */
			for (;;)
			{
				Socket client;
				
				try
				{
					client = sock.accept();
				} catch (IOException e)
				{
					if (verbose)
					{
						System.err.println("Failed to accept client: "
								+ e.getMessage());
					}
					
					continue;
				}
				
				/* Start connection handler thread */
				new TCPEcho(client).start();
			}
		}
		else if (proto.equals("udp"))
		{
			DatagramSocket sock;
			
			/* Bind */
			try
			{
				if (bindAddr != null)
				{
					/* Address specified */
					sock = new DatagramSocket(port, bindAddr);
				}
				else
				{
					/* Address unspecified */
					sock = new DatagramSocket(port);
				}
			} catch (IOException e)
			{
				System.err.println("Failed to bind: " + e.getMessage());
				return;
			}
			
			/* Send/receive forever */
			UDPEcho(sock);
		}
		else
		{
			usage();
			return;
		}
	}
}
